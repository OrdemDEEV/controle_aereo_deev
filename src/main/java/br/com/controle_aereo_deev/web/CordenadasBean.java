package br.com.controle_aereo_deev.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.controle_aereo_deev.aviao.Aviao;
import br.com.controle_aereo_deev.aviao.AviaoRN;
import br.com.controle_aereo_deev.cartesiano.Cartesiano;
import br.com.controle_aereo_deev.cartesiano.CartesianoRN;
import br.com.controle_aereo_deev.polar.PolarRN;
import br.com.controle_aereo_deev.radar.Radar;

@ManagedBean(name = "cordenadasBean")
@RequestScoped
public class CordenadasBean {
	
	
	private int X = 1600;
	private int Y = 900;
	private Aviao aviao = new Aviao();
	private Aviao aviaoSelecionado;
	private Radar radar;
	private List<Aviao> listaAviaoDataGrid;
	@ManagedProperty(value = "#{contextoBean}")
	private ContextoBean contextoBean;
	private Cartesiano cartesianoTranslacao = new Cartesiano();
	private Cartesiano cartesianoEscalonar = new Cartesiano();
	private Cartesiano cartesianoRotacao = new Cartesiano();
	private double AnguloRotacao = 0;
	private double distanciaMinimaAeroporto = 0;
	private double distanciaMinimaAvioes = 0;
	private double tempoMinimoColisao = 0;
	
	public String enviarEntradaDeDados() {
		
		
		if(this.contextoBean.getListaAviao() == null)
		{
			this.contextoBean.setListaAviao(new ArrayList<Aviao>()); 
			
		}
		
		if(this.aviao != null)
		{
			calcularDadosEntrada();
			if(this.aviao.getCodigo() == 0)
			{
				
				if(this.contextoBean.getListaAviao().size() > 0)
				{
					this.aviao.setCodigo(this.contextoBean.buscarUltimoCodigoAviao()+1);
				}else
				{
					this.aviao.setCodigo(this.aviao.getCodigo()+1);
				}
				
				this.contextoBean.getListaAviao().add(this.aviao);
				
			}else
			{
				this.contextoBean.atualizaAviaoCodigo(this.aviao);
			}
			atualizaRadar();
		}
		
		this.aviao = new Aviao();
		listaAviaoDataGrid = null;
		return null;
	}
	
	public String movimentaAvioesRadar()
	{
		AviaoRN aviaoRN = new AviaoRN();
		
		for(int i = 0; i < this.contextoBean.getListaAviao().size(); i++)
		{
			this.aviao = aviaoRN.moverAviaoFrente(this.contextoBean.getListaAviao().get(i)); 
			this.contextoBean.atualizaAviaoCodigo(this.aviao);
		}
		
		this.aviao = new Aviao();
		this.listaAviaoDataGrid = null;
		return null;
	}
	
	public String marcarSelecionado()
	{
		
		this.contextoBean.marcarSelecionado(this.aviao);
		listaAviaoDataGrid = null;
		this.aviao = new Aviao();
		return null;
	}
	
	public String excluirAviao()
	{
		this.contextoBean.removerAviao(this.aviao);
		
		listaAviaoDataGrid = null;
		this.aviao = new Aviao();
		return null;
	}
	
	public String limparSessao()
	{
		 //Contexto da Aplicação
        FacesContext conext = FacesContext.getCurrentInstance();
        //Verifica a sessao e a grava na variavel
        HttpSession session = (HttpSession) conext.getExternalContext().getSession(false);
        //Fecha/Destroi sessao
        session.invalidate();
        
        return null;
	}
	
	
	public String descobrirDistanciaMinimaAeroporto()
	{
		String relat = new String();
		AviaoRN aviaoRN = new AviaoRN();
		double distancia;
		
		
		for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
		{
			distancia = aviaoRN.calculaDistanciaAviao(this.listaAviaoDataGrid.get(i), 0, 0);
			if(this.distanciaMinimaAeroporto >= distancia)
			{
				relat += "Avião: " + this.listaAviaoDataGrid.get(i).getNome() + " esta proximo ao aeroporto " + " a " + distancia + " km\n\n";
			}	
			
		}
		
		
		if(relat.equals(""))
		{
			relat = "nenhum avião proximo ao aeroporto";
		}
		

		this.contextoBean.setRelatorio(relat);
		return null;
	}
	
	public String descobrirDistanciaMinimaAvioes()
	{
		
		String relat = new String();
		AviaoRN aviaoRN = new AviaoRN();
		double distancia;
		
			int k = 0;
			for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
			{
				
				for(k = 0; k < listaAviaoDataGrid.size(); k ++)
				{
					if(k != i)
					{
						distancia = aviaoRN.calculaDistanciaAviao(this.listaAviaoDataGrid.get(i), listaAviaoDataGrid.get(k).getCartesiano().getX(), listaAviaoDataGrid.get(k).getCartesiano().getY());
						if(this.distanciaMinimaAvioes >= distancia)
						{
							relat += "Avião " + this.listaAviaoDataGrid.get(i).getNome() + " esta proximo do ";
							relat += "avião " + this.listaAviaoDataGrid.get(k).getNome() + " a " + distancia + " km.\n\n";
						}
					}
				}
				k = i;
			}
		
		
		if(relat.equals(""))
		{
			relat = "nenhum avião proximo ao aeroporto";
		}
		

		this.contextoBean.setRelatorio(relat);
		return null;
	}
	
	
	
	public String descobrirColisao()
	{
		
		String relat = new String();
		AviaoRN aviaoRN = new AviaoRN();
		double distanciaA, distanciaB;
		double tempoAviaoA, tempoAviaoB, outrotempo;
		int k =0;
		
		for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
		{
			
			for(k = 0; k < listaAviaoDataGrid.size(); k ++)
			{
				if(k != i)
				{
					distanciaA = aviaoRN.distanciaAvioesColisao(this.listaAviaoDataGrid.get(i),this.listaAviaoDataGrid.get(k));
					distanciaB = aviaoRN.distanciaAvioesColisao(this.listaAviaoDataGrid.get(k),this.listaAviaoDataGrid.get(i));
					
					
					if(distanciaA != 0 && distanciaB != 0)
					{
						tempoAviaoA = aviaoRN.tempoAteDestino(this.listaAviaoDataGrid.get(i), distanciaA);
						tempoAviaoB = aviaoRN.tempoAteDestino(this.listaAviaoDataGrid.get(k), distanciaB);
						
						outrotempo = tempoAviaoA - tempoAviaoB;
						
						
							if(this.tempoMinimoColisao <= Math.round(outrotempo) )
							{
								relat += "Avião " + this.listaAviaoDataGrid.get(i).getNome() + " vai colidir com ";
								relat += "avião " + this.listaAviaoDataGrid.get(k).getNome() + " em " + tempoAviaoA + " Horas.\n\n";
							}
						
					}
					
					
				}
			}
			k = i;
		}
		
		
		if(relat.equals(""))
		{
			relat = "nenhum avião ira colidir";
		}
		

		this.contextoBean.setRelatorio(relat);
		return null;
	}
	
	
	public String escalonar()
	{
		AviaoRN aviaoRN = new AviaoRN();
		
		for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
		{
			
			if(this.listaAviaoDataGrid.get(i).getSelecionar().equals("Selecionado"))
			{
				this.aviaoSelecionado = aviaoRN.escalonar(this.listaAviaoDataGrid.get(i), this.cartesianoEscalonar.getX(), this.cartesianoEscalonar.getY());
		
				this.contextoBean.atualizaAviaoCodigo(aviaoSelecionado);
			}
			
		}
		
		this.listaAviaoDataGrid = null;
		this.cartesianoEscalonar = null;
		
		return null;
	}
	
	
	public String tranlacao()
	{
		AviaoRN aviaoRN = new AviaoRN();
		
		for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
		{
			
			if(this.listaAviaoDataGrid.get(i).getSelecionar().equals("Selecionado"))
			{
				this.aviaoSelecionado = aviaoRN.tranlacao(this.listaAviaoDataGrid.get(i), this.cartesianoTranslacao.getX(), this.cartesianoTranslacao.getY());
		
				this.contextoBean.atualizaAviaoCodigo(aviaoSelecionado);
			}
			
		}
		
		this.listaAviaoDataGrid = null;
		this.cartesianoTranslacao = null;
		
		return null;
	}
	
	public String rotacao()
	{
		AviaoRN aviaoRN = new AviaoRN();
		
		for(int i = 0; i < this.listaAviaoDataGrid.size(); i++)
		{
			
			if(this.listaAviaoDataGrid.get(i).getSelecionar().equals("Selecionado"))
			{
				this.aviaoSelecionado = aviaoRN.rotacionarAviao(this.listaAviaoDataGrid.get(i), this.cartesianoRotacao.getX(), this.cartesianoRotacao.getY(), this.AnguloRotacao);
		
				this.contextoBean.atualizaAviaoCodigo(aviaoSelecionado);
			}
			
		}
		
		this.listaAviaoDataGrid = null;
		this.cartesianoTranslacao = null;
		
		return null;
	}
	
	
	private boolean calcularDadosEntrada()
	{
		if( this.aviao.getCartesiano().getX() == 0.0 && this.aviao.getCartesiano().getY() == 0.0)
		{
			CartesianoRN cartesianoRN = new CartesianoRN();
			this.aviao.setCartesiano(cartesianoRN.convertCartesiano(this.aviao.getPolar()));
			
			return true;
		}else if( (int) this.aviao.getPolar().getAngulo() == 0.0 && (int) this.aviao.getPolar().getRaio() == 0.0)
		{
			PolarRN polarRN = new PolarRN();
			this.aviao.setPolar(polarRN.converterPolar(this.aviao.getCartesiano()));
			
			return true;
		}
		
		return false;
		
	}
	
	private String atualizaRadar()
	{
		this.radar = new Radar(X/2, Y/2, this.contextoBean.getListaAviao());
		return null;
	}
	
	public Radar getRadar() {
		
		if(radar == null)
		{
			if(this.contextoBean.getListaAviao() == null)
			{
				this.contextoBean.setListaAviao(new ArrayList<Aviao>()); 
				
			}
			
			this.radar = new Radar(X/2, Y/2, this.contextoBean.getListaAviao());
		}
		return radar;
	}
	

	public void setRadar(Radar radar) {
		this.radar = radar;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}


	public Aviao getAviao() {
		return aviao;
	}


	public void setAviao(Aviao aviao) {
		this.aviao = aviao;
	}


	public ContextoBean getContextoBean() {
		return contextoBean;
	}


	public void setContextoBean(ContextoBean contextoBean) {
		this.contextoBean = contextoBean;
	}

	public List<Aviao> getListaAviaoDataGrid() {
		
		if(this.listaAviaoDataGrid == null)
		{
			if(this.contextoBean.getListaAviao() == null)
			{
				this.contextoBean.setListaAviao(new ArrayList<Aviao>()); 
			}
			
			this.listaAviaoDataGrid = contextoBean.getListaAviao();
		}
		
		return listaAviaoDataGrid;
	}

	public void setListaAviaoDataGrid(List<Aviao> listaAviaoDataGrid) {
		this.listaAviaoDataGrid = listaAviaoDataGrid;
	}

	public Aviao getAviaoSelecionado() {
		return aviaoSelecionado;
	}

	public void setAviaoSelecionado(Aviao aviaoSelecionado) {
		this.aviaoSelecionado = aviaoSelecionado;
	}

	public Cartesiano getCartesianoTranslacao() {
		return cartesianoTranslacao;
	}

	public void setCartesianoTranslacao(Cartesiano cartesianoTranslacao) {
		this.cartesianoTranslacao = cartesianoTranslacao;
	}

	public Cartesiano getCartesianoEscalonar() {
		return cartesianoEscalonar;
	}

	public void setCartesianoEscalonar(Cartesiano cartesianoEscalonar) {
		this.cartesianoEscalonar = cartesianoEscalonar;
	}

	public Cartesiano getCartesianoRotacao() {
		return cartesianoRotacao;
	}

	public void setCartesianoRotacao(Cartesiano cartesianoRotacao) {
		this.cartesianoRotacao = cartesianoRotacao;
	}

	public double getAnguloRotacao() {
		return AnguloRotacao;
	}

	public void setAnguloRotacao(double anguloRotacao) {
		AnguloRotacao = anguloRotacao;
	}

	public double getDistanciaMinimaAeroporto() {
		return distanciaMinimaAeroporto;
	}

	public void setDistanciaMinimaAeroporto(double distanciaMinimaAeroporto) {
		this.distanciaMinimaAeroporto = distanciaMinimaAeroporto;
	}

	public double getDistanciaMinimaAvioes() {
		return distanciaMinimaAvioes;
	}

	public void setDistanciaMinimaAvioes(double distanciaMinimaAvioes) {
		this.distanciaMinimaAvioes = distanciaMinimaAvioes;
	}

	public double getTempoMinimoColisao() {
		return tempoMinimoColisao;
	}

	public void setTempoMinimoColisao(double tempoMinimoColisao) {
		this.tempoMinimoColisao = tempoMinimoColisao;
	}
	
	
	
	
	
}