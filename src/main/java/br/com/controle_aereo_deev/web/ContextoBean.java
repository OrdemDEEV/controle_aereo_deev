package br.com.controle_aereo_deev.web;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import br.com.controle_aereo_deev.aviao.Aviao;

@ManagedBean
@SessionScoped
public class ContextoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2098826321480557978L;
	
	
	private List<Aviao> listaAviao = null;
	private String relatorio = new String();
	
	public List<Aviao> getListaAviao() {
		return listaAviao;
	}

	public void setListaAviao(List<Aviao> listaAviao) {
		this.listaAviao = listaAviao;
	}
	
	public String getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}

	public void atualizaAviaoCodigo(Aviao aviao)
	{
		for(int i=0; i < listaAviao.size(); i ++)
		{
			if(listaAviao.get(i).getCodigo() == aviao.getCodigo())
			{
				this.listaAviao.get(i).setCartesiano(aviao.getCartesiano());
				this.listaAviao.get(i).setPolar(aviao.getPolar());
				this.listaAviao.get(i).setNome(aviao.getNome());
				this.listaAviao.get(i).setDirecao(aviao.getDirecao());
				this.listaAviao.get(i).setVelocidade(aviao.getVelocidade());
				this.listaAviao.get(i).setSelecionar(aviao.getSelecionar());
			}
		}
	}
	
	
	public void removerAviao(Aviao aviao)
	{
		for(int i = 0; i < listaAviao.size(); i++)
		{
			if(listaAviao.get(i).getCodigo() == aviao.getCodigo())
			{
				listaAviao.remove(i);
			}
		}
	}
	
	public void marcarSelecionado(Aviao aviao)
	{
		for(int i = 0; i < listaAviao.size(); i++)
		{
			if(listaAviao.get(i).getCodigo() == aviao.getCodigo())
			{
				if(listaAviao.get(i).getSelecionar().equals("Selecionar"))
				{
					listaAviao.get(i).setSelecionar("Selecionado");;
				}else
				{
					listaAviao.get(i).setSelecionar("Selecionar");;
				}
			}
		}
	}
	
	public int buscarUltimoCodigoAviao()
	{
		return listaAviao.get(listaAviao.size() -1).getCodigo();
	}

}