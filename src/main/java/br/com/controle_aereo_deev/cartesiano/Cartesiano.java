package br.com.controle_aereo_deev.cartesiano;

import java.io.Serializable;

public class Cartesiano implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1968109223877156326L;
	
	private double x;
	private double y;
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	

}
