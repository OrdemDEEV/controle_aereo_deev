package br.com.controle_aereo_deev.cartesiano;

import br.com.controle_aereo_deev.polar.Polar;

public class CartesianoRN {
	
	
	public Cartesiano convertCartesiano(Polar polar)
	{
		Cartesiano cartesiano = new Cartesiano();
		
		cartesiano.setX(polar.getRaio() *Math.cos(polar.getAngulo()));
		cartesiano.setY(polar.getRaio() *Math.sin(polar.getAngulo()));
		
		return cartesiano;
	}
	
	 public double calculaCoeficientLinear(double coeficienteAngular, double x, double y) {
	     return y - coeficienteAngular * x;
	 }
	
}
