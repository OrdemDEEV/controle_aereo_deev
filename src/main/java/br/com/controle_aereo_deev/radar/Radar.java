package br.com.controle_aereo_deev.radar;
import java.util.List;

import br.com.controle_aereo_deev.aviao.Aviao;
import net.bootsfaces.component.canvas.Drawing;

public class Radar extends Drawing {
	
	
	
	public Radar(int x, int y)
	{ //  text(805+210+10, 470, "innovative", "16px Arial");
		
		
		
		    circle(x, y, 420);
		    circle(x, y, 315);
		    circle(x, y,  210);
		    circle(x, y,  105);
		    line(x-y, y, x+y, y);
		    line(x, 0, x, 900);
		    
		    
		    
		    //botar riscos X
		    int interador = 10;
		    for(int i = 0; i < 43; i++)
		    {
		    	 line(x+interador, y-5, x+interador, y+5);
		    	 line(x-interador, y-5, x-interador, y+5);
		    	 interador += 10;
		    	
		    }
		    
		  //botar riscos Y
		    interador = 10;
		    for(int i = 0; i < 43; i++)
		    {
		    	 line(x-5, y+interador, x+5, y+interador);
		    	 line(x-5, y-interador, x+5, y-interador);
		    	 interador += 10;
		    }
		   
		 
		    filledCircle(805+40+70, 470-105-20,5, "green");
		
		    
		    filledCircle(805+70+105, 470-210-10,5, "blue");
	}
	
	public Radar(int x, int y, List<Aviao> listaAviao)
	{ //  text(805+210+10, 470, "innovative", "16px Arial");
		
		
		
		    circle(x, y, 420);
		    circle(x, y, 315);
		    circle(x, y,  210);
		    circle(x, y,  105);
		    line(x-y, y, x+y, y);
		    line(x, 0, x, 900);
		    
		    
		    //botar riscos X
		    int e=1;
		    float f = new Float(0.50);
		    int interador = 30;
		   
		    for(int i = 0; i < 15; i++)
		    {
		    	
		    	//riscos maiores
		    	 line(x+interador, y-10, x+interador, y+10);
		    	 line(x-interador, y-10, x-interador, y+10);
		    	 
		    	 //numero riscos maiores
		    	 text(x+(interador-5), y+30 , String.valueOf(e), "16px Arial"); //positivo
		    	 text(x-(interador+11), y+30 , "-"+String.valueOf(e), "16px Arial"); //negativo
	    	    
		    	 
	    	     
		    	 //riscos menores
		    	 line(x+(interador-15), y-5, x+(interador-15), y+5);
		    	 line(x-(interador-15), y-5, x-(interador-15), y+5);
		         
		    	 //numero riscos menores
	    	     text(x+(interador-20), y+15 , String.valueOf(f), "10px Arial"); //positivo
	    	     text(x-(interador), y+15 , "-"+String.valueOf(f), "10px Arial"); //negativo
		    	 
		    	 f += 1;
		    	 e += 1;
		    	 interador += 30;

		    }
		    
		  //botar riscos Y
		    interador = 30;
		    e=1;
		    f= new Float(0.50);
		    for(int i = 0; i < 14; i++)
		    {
		    	
		    	//riscos maiores
		    	 line(x-10, y+interador, x+10, y+interador);
		    	 line(x-10, y-interador, x+10, y-interador);
		    	 
		    	 
		    	 text(x-35, y-(interador-5), String.valueOf(e), "16px Arial"); //positivo
		    	 text(x-35, y+(interador+7) , "-"+String.valueOf(e), "16px Arial"); //negativo
		    	
		    	//riscos menores
		    	 line(x-5, y+(interador-15), x+5, y+(interador-15));
		    	 line(x-5, y-(interador-15), x+5, y-(interador-15));
		    	 
		    	 
		    	 text(x-25, y-(interador-15), String.valueOf(f), "10px Arial"); //positivo
		    	 text(x-25, y+(interador-7) , "-"+String.valueOf(f), "10px Arial"); //negativo
		    	 
		    	 f += 1;
		    	 e += 1;
		    	 interador += 30;
		    }
		   
		    
		  if(listaAviao != null)
		  {
			  
			    double calcX;
				double calcY;
				double novoX;
				double novoY;
			  
			  for(int i=0; i < listaAviao.size(); i++)
			  {
				  filledCircle(x+(int)(listaAviao.get(i).getCartesiano().getX()*30.0), y- (int)(listaAviao.get(i).getCartesiano().getY()*30.0),5, "red");
				  text(x+(int)(listaAviao.get(i).getCartesiano().getX()*30.0), y-(int)((listaAviao.get(i).getCartesiano().getY()*30.0)-16), listaAviao.get(i).getNome(), "16px Arial");
					
				  
				  //calcula dire��o
					calcX = (listaAviao.get(i).getCartesiano().getX()+0.5) - listaAviao.get(i).getCartesiano().getX();
					calcY = (listaAviao.get(i).getCartesiano().getY()) - listaAviao.get(i).getCartesiano().getY();

					novoX = calcX*Math.cos(Math.toRadians(listaAviao.get(i).getDirecao())) - calcY * Math.sin(Math.toRadians(listaAviao.get(i).getDirecao()));
					novoY = calcY*Math.cos(Math.toRadians(listaAviao.get(i).getDirecao())) + calcX * Math.sin(Math.toRadians(listaAviao.get(i).getDirecao()));
					
					novoX += listaAviao.get(i).getCartesiano().getX();
					novoY += listaAviao.get(i).getCartesiano().getY();
				  
					//coloca dire��o
					line(x+(int)(listaAviao.get(i).getCartesiano().getX()*30.0), y-(int)(listaAviao.get(i).getCartesiano().getY()*30.0), x+(int)(novoX*30.0), y-(int)(novoY*30.0));
					
				  
			  }
		  }
		  
		  
		    
	}
	
	

}