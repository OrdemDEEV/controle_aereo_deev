package br.com.controle_aereo_deev.aviao;

import br.com.controle_aereo_deev.cartesiano.Cartesiano;
import br.com.controle_aereo_deev.polar.Polar;

public class Aviao{
	
	private int codigo = 0;
	private Cartesiano cartesiano = new Cartesiano();
	private Polar polar = new Polar();
	private String nome;
	private double Velocidade;
	private double direcao;
	private String selecionar = new String("Selecionar");
	
	public Cartesiano getCartesiano() {
		return cartesiano;
	}
	public void setCartesiano(Cartesiano cartesiano) {
		this.cartesiano = cartesiano;
	}
	public Polar getPolar() {
		return polar;
	}
	public void setPolar(Polar polar) {
		this.polar = polar;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getVelocidade() {
		return Velocidade;
	}
	public void setVelocidade(double velocidade) {
		Velocidade = velocidade;
	}
	public double getDirecao() {
		return direcao;
	}
	public void setDirecao(double direcao) {
		this.direcao = direcao;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getSelecionar() {
		return selecionar;
	}
	public void setSelecionar(String selecionar) {
		this.selecionar = selecionar;
	}
	
	
	
	
	
	
	
	

	
	
}