package br.com.controle_aereo_deev.aviao;

import br.com.controle_aereo_deev.cartesiano.CartesianoRN;
import br.com.controle_aereo_deev.polar.PolarRN;



public class AviaoRN {
	
	public Aviao tranlacao(Aviao aviao, double x, double y)
	{	
		Aviao novoAviao = aviao;
		
		novoAviao.getCartesiano().setX(aviao.getCartesiano().getX()+x);
		novoAviao.getCartesiano().setY(aviao.getCartesiano().getY()+y);

		PolarRN polarRN = new PolarRN();
		novoAviao.setPolar(polarRN.converterPolar(novoAviao.getCartesiano()));
		
		return novoAviao;
	}
	
	public Aviao escalonar(Aviao aviao, double x, double y)
	{	
		Aviao novoAviao = aviao;
		
		if(aviao.getCartesiano().getX() < 0)
		{
			//aviao.getCartesiano().setX(aviao.getCartesiano().getX()*-1);
		}
		
		if(aviao.getCartesiano().getY() < 0)
		{
			//aviao.getCartesiano().setY(aviao.getCartesiano().getY()*-1);
		}
		
		novoAviao.getCartesiano().setX(aviao.getCartesiano().getX()*(x));
		novoAviao.getCartesiano().setY(aviao.getCartesiano().getY()*(y));

		PolarRN polarRN = new PolarRN();
		novoAviao.setPolar(polarRN.converterPolar(novoAviao.getCartesiano()));
		
		return novoAviao;
	}
    
	
	public Aviao rotacionarAviao(Aviao aviao, double x, double y, double angulo)
	{
		Aviao novoAviao = aviao;
		double calcX;
		double calcY;
		
		calcX = aviao.getCartesiano().getX() - x;
		calcY = aviao.getCartesiano().getY() - y;

		novoAviao.getCartesiano().setX( calcX*Math.cos(Math.toRadians(angulo)) - calcY * Math.sin(Math.toRadians(angulo)));
		novoAviao.getCartesiano().setY( calcY*Math.cos(Math.toRadians(angulo)) + calcX * Math.sin(Math.toRadians(angulo)));
		
		novoAviao.getCartesiano().setX(novoAviao.getCartesiano().getX() + x);
		novoAviao.getCartesiano().setY(novoAviao.getCartesiano().getY() + y);
		
		PolarRN polarRN = new PolarRN();
		novoAviao.setPolar(polarRN.converterPolar(novoAviao.getCartesiano()));
		
		
		return novoAviao;
		

	}
	
	
	public double calculaDistanciaAviao(Aviao aviao, double x, double y)
	{
		
		double distanciax, distanciay, distancia;
		
		distanciax = Math.pow((x - aviao.getCartesiano().getX()), 2);
		distanciay = Math.pow((y - aviao.getCartesiano().getY()), 2);
		distancia = Math.sqrt(distanciax+distanciay);
		
		return distancia;
	}
	
	public Aviao moverAviaoFrente(Aviao aviao)
	{	
		double calcX;
		double calcY;
		double novoX;
		double novoY;
		
		calcX = (aviao.getCartesiano().getX()+aviao.getVelocidade()) - aviao.getCartesiano().getX();
		calcY = (aviao.getCartesiano().getY()) - aviao.getCartesiano().getY();

		novoX = calcX*Math.cos(Math.toRadians(aviao.getDirecao())) - calcY * Math.sin(Math.toRadians(aviao.getDirecao()));
		novoY = calcY*Math.cos(Math.toRadians(aviao.getDirecao())) + calcX * Math.sin(Math.toRadians(aviao.getDirecao()));
		
		novoX += aviao.getCartesiano().getX();
		novoY += aviao.getCartesiano().getY();
		
		aviao.getCartesiano().setX(novoX);
		aviao.getCartesiano().setY(novoY);
		
		return aviao;
		
	}
	
	
	public double distanciaAvioesColisao(Aviao aviaoA, Aviao aviaoB)
	{
		double x, y;
		double angularA, angularB;
        double linearA, linearB;
        double distancia = 0;
		
        CartesianoRN cartesianoRN = new CartesianoRN();
        PolarRN polarRN = new PolarRN();
        
        
        angularA = polarRN.calculaCoeficientAngular(aviaoA.getDirecao());
        angularB = polarRN.calculaCoeficientAngular(aviaoB.getDirecao());
        
        linearA =  cartesianoRN.calculaCoeficientLinear(angularA, aviaoA.getCartesiano().getX(), aviaoA.getCartesiano().getY());
        linearB =  cartesianoRN.calculaCoeficientLinear(angularB, aviaoB.getCartesiano().getX(), aviaoB.getCartesiano().getY());
	
        
        x = (linearB - (linearA)) / (angularA - angularB );
		y = angularA * aviaoB.getCartesiano().getX() + linearB;
		
		
		//sempre pegar o maior positivo
		if(x  <= y)
		{
			
			if(x < 0)
			{
				distancia = y;
			}else
			{
				distancia = x;
			}
		}else
		{
			if(y < 0)
			{
				distancia = x;
				
			}else
			{
				distancia = y;
			}
		}
		
        
		if(Double.isInfinite(x) || Double.isInfinite(y))
		{
			distancia = 0;
		}
		
		if(aviaoA.getDirecao()%360 > 0 && aviaoA.getDirecao()%360 < 180){
            if(aviaoA.getCartesiano().getY() > y){
                return 0;
            }
        }
        else if(aviaoA.getDirecao()%360 > 180){
            if(aviaoA.getCartesiano().getY() < y){
                return 0;
            }
        }
        
        else{
            if(aviaoA.getDirecao()%360 == 0){
                if( aviaoA.getCartesiano().getX() > x){
                    return 0;
                }
            }
            
            if(aviaoA.getDirecao()%360 == 180){
                if(aviaoA.getCartesiano().getX() < x){
                   return 0; 
                }
            }
        }
		
		
		return distancia;
	}
	
	
	public double tempoAteDestino(Aviao aviao, double distancia)
	{
		return distancia/aviao.getVelocidade();
	}
	
	
}
