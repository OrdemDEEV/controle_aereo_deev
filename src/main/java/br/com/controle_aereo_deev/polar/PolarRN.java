package br.com.controle_aereo_deev.polar;

import br.com.controle_aereo_deev.cartesiano.Cartesiano;

public class PolarRN {
	
	public Polar converterPolar(Cartesiano cartesiano)
	{
		Polar polar = new Polar();
		Double raio = 0.0;
		Double angulo = 0.0;
		
		raio = Math.sqrt(Math.pow(cartesiano.getX(), 2) + Math.pow(cartesiano.getY(), 2));
		angulo = Math.atan(cartesiano.getX()/cartesiano.getY());
		
		polar.setRaio(raio);
		polar.setAngulo(angulo);
		
		return polar;
	}
	
	public double calculaCoeficientAngular(double angulo) {
        return Math.tan(Math.toRadians(angulo));
    }
	

}
